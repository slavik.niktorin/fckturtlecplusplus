#include <chrono>
#include <functional>
#include <memory>
#include <iostream>
#include <string>

#include "rclcpp/rclcpp.hpp"
#include "geometry_msgs/msg/twist.hpp"
#include "turtlesim/msg/pose.hpp"
#include <math.h>  

using namespace std::chrono_literals;
using std::placeholders::_1;
using std::cout;
using std::cin;
using std::endl;

const double DELTA = 0.01;


double ang, coef, cor_ang;
struct point_t{
  double x;
  double y;
  double orient;
  
  

  
};


class FckTurtle :  public rclcpp::Node
{
  public:
    FckTurtle()
    : Node("talker")
    {
      publisher_ = this->create_publisher<geometry_msgs::msg::Twist>("/turtle1/cmd_vel", 10);
        timer_ = this->create_wall_timer(
      10ms, std::bind(&FckTurtle::timer_callback, this));
    
       subscription_ = this->create_subscription<turtlesim::msg::Pose>(
      "/turtle1/pose", 10, std::bind(&FckTurtle::topic_callback, this, _1));
    }
    /* void setPoints(point_t point){
      tar_point_=point;
    }
     point_t getTarPoint(){
      return (tar_point_);
     } */
    
  private:
   

    
    
    point_t curr_pose_{0, 0, 0};
    point_t tar_point_{5.54, 5.54, 0};
    
     
    
    void topic_callback(const turtlesim::msg::Pose::SharedPtr msg) 
    { 
    
    
    curr_pose_.x = msg->x;
    curr_pose_.y = msg->y;
    curr_pose_.orient = msg->theta;
    

    }
    rclcpp::Subscription<turtlesim::msg::Pose>::SharedPtr subscription_;
    
    void timer_callback()

     {
       
  geometry_msgs::msg::Twist cmd;       
   
       double tg=((tar_point_.y)-(curr_pose_.y))/((tar_point_.x)-(curr_pose_.x));
  if ((tar_point_.y>=curr_pose_.y)&&(tar_point_.x>=curr_pose_.x)){
     cor_ang= atan(tg);
    }else {
      if ((tar_point_.y>=curr_pose_.y)&&(tar_point_.x<=curr_pose_.x)){
       cor_ang= atan(tg)+M_PI;
        }else {
         if((tar_point_.y<=curr_pose_.y)&&(tar_point_.x<=curr_pose_.x)){
           cor_ang= atan(tg)-M_PI;
         }else {
            
              cor_ang= atan(tg);
          }}}
  
      cout<<"tar_point_x= "<< tar_point_.x<<endl<<"tar_point_y= "<<tar_point_.y<<endl;
      cout << "Cor_Ang = "<< cor_ang << endl << "Turtle angle = " << curr_pose_.orient << endl;
       if (std::abs(cor_ang - curr_pose_.orient) < 0.1)
     {
         cmd.angular.z=0;
         

        if (std::abs(tar_point_.x - curr_pose_.x)< DELTA && abs(tar_point_.y - curr_pose_.y)<DELTA){
                cmd.linear.x = 0;
                cout << "X= ";
                cin >> tar_point_.x;
                cout << "Y= ";
                cin >> tar_point_.y; 
        
            
        }







        else {
                cmd.linear.x = 1;
        }

     } else {
         cmd.angular.z=0.5;

         } 

     
      publisher_->publish(cmd);
      
     }        
    rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr publisher_;
    rclcpp::TimerBase::SharedPtr timer_;
     

  }; 

  int main(int argc, char * argv[])
  {
    
    
    rclcpp::init(argc, argv);
   /* point_t test;
    std::cout << "X= ";
    std::cin >> test.x;
    std::cout << "Y= ";
    std::cin >> test.y; 
    std::shared_ptr<rclcpp::Node> node = rclcpp::Node::make_shared("add_two_ints_server"); */

   // FckTurtle turtle;
   // turtle.setPoints(test);
    rclcpp::spin(std::make_shared<FckTurtle>());
    //auto turtle = std::make_shared<FckTurtle>();
   // turtle->setPoints(test);
    //rclcpp::spin(turtle);
    rclcpp::shutdown();
    return 0;
   }