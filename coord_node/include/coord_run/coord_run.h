#include "rclcpp/rclcpp.hpp"
#include "geometry_msgs/Point.msg"
#include "turtlesim/msg/pose.hpp"


class turtleControl :  public rclcpp::Node
{
private:
    /* data */
    rclcpp::Publisher<geometry_msgs::msg::Point>::SharedPtr coord_pub_;
    rclcpp::Subscription<turtlesim::msg::Pose>::SharedPtr pose_sub_;
    void topic_callback(const turtlesim::msg::Pose::SharedPtr msg);

   
public:
    turtleControl();
    ~turtleControl();

};

