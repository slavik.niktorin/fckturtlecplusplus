#include"coord_run.h"



turtleControl::turtleControl(/* args */)
{
    coord_pub_= this->create_publisher<geometry_msgs::msg::Point>("target_point", 10); //поле данных класса
     pose_sub_= this->create_subscription<turtlesim::msg::Pose>(
      "/turtle1/pose", 10, std::bind(&FckTurtle::topic_callback, this, _1));
}



turtleControl::~turtleControl()
{
}

void turtleControl::topic_callback(const turtlesim::msg::Pose::SharedPtr msg) 
    { 
    
    
    curr_pose_.x = msg->x;
    curr_pose_.y = msg->y;
    curr_pose_.orient = msg->theta;
    

    }