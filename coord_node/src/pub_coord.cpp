#include <chrono>
#include <functional>
#include <memory>
#include <iostream>
#include <string>

#include "rclcpp/rclcpp.hpp"
#include "geometry_msgs/msg/twist.hpp"
#include "turtlesim/msg/pose.hpp"
#include <math.h>  

using namespace std::chrono_literals;
using std::placeholders::_1;
using std::cout;
using std::cin;
using std::endl;
